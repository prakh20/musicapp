import React from "react";
import {
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator
} from "react-navigation";
import { TouchableOpacity } from "react-native";
import { Icon, Avatar } from "react-native-elements";
import Home from "../components/Home";
import Hotlist from "../components/Hotlist";
import Library from "../components/Library";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import Profile from "../modals/Profile";

const ACTIVE_TINT_COLOR = "#fff";

const TabNavigator = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="ios-home" type="ionicon" size={20} color={tintColor} />
        )
      },
      tabBarOptions: {
        activeTintColor: ACTIVE_TINT_COLOR
      }
    },
    Hotlist: {
      screen: Hotlist,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name={"ios-flame"} color={tintColor} type="ionicon" size={20} />
        ),
        tabBarOptions: {
          activeTintColor: ACTIVE_TINT_COLOR
        }
      }
    },
    Library: {
      screen: Library,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name={"ios-musical-notes"}
            type="ionicon"
            size={20}
            color={tintColor}
          />
        )
      },
      tabBarOptions: {
        activeTintColor: ACTIVE_TINT_COLOR
      }
    }
  },
  {
    initialRouteName: "Home",
    activeColor: "#fff",
    inactiveColor: "#808080",
    barStyle: { backgroundColor: "#282828" }
  }
);

const MainStack = createStackNavigator(
  {
    tabNavigator: { screen: TabNavigator }
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: "#282828"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold"
      },
      headerRight: (
        <TouchableOpacity
          style={{ marginRight: 10 }}
          onPress={() => {
            this.callProfile._toggleState();
          }}
        >
          <Avatar rounded title="MD" size={"small"} />
          <Profile
            ref={refer => {
              this.callProfile = refer;
            }}
          />
        </TouchableOpacity>
      )
    }
  }
);

const AppNavigator = createSwitchNavigator({
  mainStack: { screen: MainStack }
});

export default createAppContainer(AppNavigator);
