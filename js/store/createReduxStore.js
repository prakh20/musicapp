import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

export default function createReduxStore() {
  const enhancer = compose(applyMiddleware(thunk));
  let store = createStore(enhancer);
  return store;
}
