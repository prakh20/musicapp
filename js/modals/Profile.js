import React, { Component } from "react";
import { View, Text, Modal } from "react-native";

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  _toggleState = () => {
    this.setState({ visible: !this.state.visible });
  };

  render() {
    return (
      <Modal
        visible={this.state.visible}
        onRequestClose={this._toggleState}
        animationType={"slide"}
      >
        <View>
          <Text> Profile </Text>
        </View>
      </Modal>
    );
  }
}
